# Schema Registry

The Schema Registry components is a context json registry.

It provides a swagger on ```/api/docs``` endpoint.

With this API you can discover which context is available.

To configure exposed context, you need to change the deployment configuration.