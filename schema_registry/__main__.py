# -*- coding: utf-8 -*-
import asyncio
import logging
import os
from contextlib import asynccontextmanager

import uvicorn
from fastapi import FastAPI
from schema_registry.Config import ROOT_DIR, Settings, get_settings
from schema_registry.infra.api.rest.api import create_fastapi_application
from schema_registry.observability.logger import configure_logging
from prometheus_fastapi_instrumentator import Instrumentator

from schema_registry.services.schema_service import SchemaService

if __name__ == "__main__":
    settings: Settings = get_settings()
    configure_logging()

    schema_service = SchemaService()

    @asynccontextmanager
    async def lifespan(app: FastAPI):
        log = logging.getLogger(__name__)
        log.info(f"Healthcheck endpoint available on : http://127.0.0.1:{settings.api_port_exposed}/healthcheck")
        instrumentator.expose(app)
        yield
        log.info("Shutting down")

    app = create_fastapi_application(
        lifespan=lifespan,
        schema_service=schema_service
    )

    logging.debug(f"Context Map: {settings.context_map}")

    instrumentator = Instrumentator().instrument(app)
    uvicorn.run(app, host="0.0.0.0", port=settings.api_port_exposed, log_config=None)
