# -*- coding: utf-8 -*-

from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from schema_registry.infra.api.rest.constants import (
    API_DESCRIPTION,
    API_TITLE,
    TAGS_METADATA,
)
from schema_registry.infra.api.rest.routes.common import create_common_router
from schema_registry.infra.api.rest.routes.schema import create_schema_router
from schema_registry.services.schema_service import SchemaService


def create_fastapi_application(
    lifespan,
    schema_service: SchemaService
) -> FastAPI:
    """
    The create_fastapi_application function creates a FastAPI application object.

    Args:

    Returns:
        A fastapi object
    """

    app = FastAPI(
        lifespan=lifespan,
        docs_url="/api/doc",
        redoc_url=None,
        title=API_TITLE,
        openapi_tags=TAGS_METADATA,
        description=API_DESCRIPTION,
        version="22.10",
        license_info={
            "name": "Apache 2.0",
            "url": "https://www.apache.org/licenses/LICENSE-2.0.html",
        },
    )

    app.add_middleware(
        CORSMiddleware,
        allow_origins=["*"],
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )

    app.include_router(router=create_common_router(), tags=["Common"])
    app.include_router(router=create_schema_router(schema_service=schema_service), tags=["Schema"])

    return app
