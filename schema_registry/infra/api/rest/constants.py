# -*- coding: utf-8 -*-
from typing import Final

API_TITLE: Final = "Schema Registry"
API_DESCRIPTION: Final = """
REST API of the GXFS-FR Schema Registry

[Source repository](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/catalogue/schema-registry)
"""

TAGS_METADATA: Final = [
    # {
    #     "name": "JSON-LD",
    #     "description": "Post JSON-LD object into the Catalogue encapsulated into graph object.",
    # },
    # {"name": "Query", "description": "Send graph queries to catalogue."},
    # {"name": "References", "description": "Get references data of the federation."},
    # {"name": "Catalogue synchronisation", "description": "API to synchronise catalogue"},
]
