# -*- coding: utf-8 -*-
import http
import logging
from http import HTTPStatus

from fastapi import APIRouter, Request
from fastapi.responses import JSONResponse

from schema_registry.services.schema_service import SchemaService, KeyNotFound

log = logging.getLogger(__name__)


def create_schema_router(schema_service: SchemaService) -> APIRouter():
    """
    The create_schema_router function is used to create a router for the schema routes.

    Args:

    Returns:
        A router
    """
    schema_router = APIRouter()

    @schema_router.get("/contexts/", status_code=http.HTTPStatus.OK)
    async def get_context_keys():
        """
        List the keys inside the schema_registry

        :return:
        """

        return JSONResponse({
            "available_keys": schema_service.list_context_keys()
        })

    @schema_router.get("/contexts/{context_key}", status_code=http.HTTPStatus.OK)
    async def get_context_by_key(context_key: str, request: Request):
        """
        Get a context according to a key.

        :return:
        """

        try:
            target_context = await schema_service.retrieve_context(context_key, f"{request.url}")

            return JSONResponse(target_context, media_type="application/ld+json")
        except KeyNotFound as error:
            return JSONResponse({
                "message": f"{error}"
            }, status_code=HTTPStatus.NOT_FOUND)

    return schema_router
