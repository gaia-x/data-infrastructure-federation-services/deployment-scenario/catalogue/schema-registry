# -*- coding: utf-8 -*-
import http
import logging

from fastapi import APIRouter, Request
from fastapi.responses import JSONResponse

log = logging.getLogger(__name__)


def create_common_router() -> APIRouter():
    """
    The create_common_router function is used to create a router for the common routes.

    Args:

    Returns:
        A router
    """
    common_router = APIRouter()

    @common_router.get("/", status_code=http.HTTPStatus.OK)
    def root_info(request: Request):
        """
        The root function is used to share usage information like doc url.
        :return:
        """

        data = {"healthcheck": f"{str(request.url)}healthcheck", "url_swagger": f"{str(request.url)}api/doc"}

        return JSONResponse(content=data)

    @common_router.get("/healthcheck", status_code=http.HTTPStatus.OK)
    async def healthcheck():
        """
        The healthcheck function is used to check the health of the Schema Registry.
        It returns a string indicating that it is OK.

        Args:

        Returns:
            A string
        """
        statuses = {
            "health": "ALIVE"
        }

        return JSONResponse(content=statuses)

    return common_router
