# -*- coding: utf-8 -*-
import os
from enum import Enum
from functools import lru_cache
from typing import Final

from dotenv import load_dotenv
from pydantic_settings import BaseSettings
from pydantic.types import SecretStr

load_dotenv()

ROOT_DIR: Final = os.path.realpath(os.path.dirname(__file__))


class LogLevelEnum(str, Enum):
    critical = "CRITICAL"
    fatal = "FATAL"
    error = "ERROR"
    warning = "WARNING"
    info = "INFO"
    debug = "DEBUG"


class EnvironmentEnum(str, Enum):
    dev = "DEV"
    prod = "PROD"
    test = "TEST"


class Settings(BaseSettings):
    log_level: LogLevelEnum = LogLevelEnum.debug
    environment: EnvironmentEnum = EnvironmentEnum.dev
    api_port_exposed: int = 8080
    context_map: dict = {}

#    ces_url: AnyHttpUrl = "http://127.0.0.1"
#    parent_domain: str

    @classmethod
    def apply_root(cls, value: str):
        """
        The apply_root function is a class method that takes in a string value and returns the same string with
        whitespace stripped from both ends of the string. If the input value is None, then it will return None.

        Args:
            cls: Pass the class of the object that is being created
            value: str: Define the type of value that will be passed to the function

        Returns:
            The value of the field if it is none

        """
        return value if value is None else value.strip().replace("\n", "")


@lru_cache
def get_settings() -> Settings:
    """
    The get_settings function is a factory function that returns an instance of the Settings class.
    The Settings class contains all the settings for this project, and it's used by other modules to access
    those settings.

    Args:

    Returns:
        A settings object
    """
    return Settings()
