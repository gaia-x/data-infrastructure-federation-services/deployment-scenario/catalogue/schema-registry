import json
import logging

from schema_registry.Config import Settings, get_settings
from schema_registry.utils import get_json_from_url

log = logging.getLogger(__name__)
settings: Settings = get_settings()


class SchemaService:
    CACHE_CONTEXT_MAP = {}

    def __init__(self) -> None:
        super().__init__()

    async def load_context_files(self):
        for key in settings.context_map:
            self.CACHE_CONTEXT_MAP[key] = await get_json_from_url(settings.context_map[key])

    async def retrieve_context(self, key, target_url: str):
        target_url = self.clean_url_to_push_it_into_context(target_url, key)

        if key in self.CACHE_CONTEXT_MAP:
            base_context = self.CACHE_CONTEXT_MAP[key]
        elif key in settings.context_map:
            base_context = await get_json_from_url(settings.context_map[key])
            self.CACHE_CONTEXT_MAP[key] = base_context
        else:
            logging.error(f"Unable to retrieve context for {key}")
            raise KeyNotFound(f"Unable to retrieve context for {key}")

        target_context = json.dumps(base_context).replace("$BASE_URL$", target_url)

        return json.loads(target_context)

    def clean_url_to_push_it_into_context(self, url: str, key):
        return url.removeprefix("http://").removeprefix("https://").removesuffix("/").removesuffix(key).removesuffix("/")

    def list_context_keys(self) -> list:
        return list(settings.context_map.keys())


class KeyNotFound(Exception):
    """
    The KeyNotFound class is a custom exception class that is raised when a key of context is not found. It inherits
    from the base Exception class and takes a message as input.
    """

    def __init__(self, message: str) -> None:
        super().__init__(message)
